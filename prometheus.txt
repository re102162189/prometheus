udo mkdir /etc/prometheus
sudo mkdir /var/lib/prometheus

sudo chown apadmin:apadmin /etc/prometheus
sudo chown apadmin:apadmin /var/lib/prometheus

cd ~
tar xvfz prometheus-2.3.2.linux-amd64.tar.gz

sudo cp prometheus-2.3.2.linux-amd64/prometheus /usr/local/bin/
sudo cp prometheus-2.3.2.linux-amd64/promtool /usr/local/bin/

sudo chown apadmin:apadmin /usr/local/bin/prometheus
sudo chown apadmin:apadmin /usr/local/bin/promtool

sudo cp -r prometheus-2.3.2.linux-amd64/consoles /etc/prometheus
sudo cp -r prometheus-2.3.2.linux-amd64/console_libraries /etc/prometheus

sudo chown -R apadmin:apadmin /etc/prometheus/consoles
sudo chown -R apadmin:apadmin /etc/prometheus/console_libraries

rm -rf prometheus-2.3.2.linux-amd64.tar.gz prometheus-2.0.0.linux-amd64

 

sudo vi /etc/prometheus/prometheus.yml

global:
scrape_interval: 15s
evaluation_interval: 15s

alerting:
alertmanagers:
- static_configs:
- targets:
# - alertmanager:9093

# Load rules once and periodically evaluate them according to the global 'evaluation_interval'.
rule_files:
# - "first_rules.yml"
# - "second_rules.yml"

scrape_configs:
- job_name: 'prometheus'
static_configs:
- targets: ['172.26.15.53:9100', '172.26.11.203:9100']


sudo chown apadmin:apdmin /etc/prometheus/prometheus.yml


sudo vi /etc/systemd/system/prometheus.service

[Unit]
Description=Prometheus
Wants=network-online.target
After=network-online.target

[Service]
User=apadmin
Group=apadmin
Type=simple
ExecStart=/usr/local/bin/prometheus \
    --config.file /etc/prometheus/prometheus.yml \
    --storage.tsdb.path /var/lib/prometheus/ \
    --web.console.templates=/etc/prometheus/consoles \
    --web.console.libraries=/etc/prometheus/console_libraries

[Install]
WantedBy=multi-user.target


sudo systemctl daemon-reload

sudo systemctl start prometheus

sudo systemctl status prometheus

sudo systemctl enable prometheus

